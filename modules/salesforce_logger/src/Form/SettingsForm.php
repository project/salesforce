<?php

namespace Drupal\salesforce_logger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce\Event\SalesforceEvents;

/**
 * Creates authorization form for Salesforce.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'salesforce_logger.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'salesforce_logger.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('salesforce_logger.settings');

    $form['log_level'] = [
      '#title' => $this->t('Salesforce Logger log level'),
      '#type' => 'radios',
      '#options' => [
        SalesforceEvents::ERROR => $this->t('Log errors only'),
        SalesforceEvents::WARNING => $this->t('Log warnings and errors'),
        SalesforceEvents::NOTICE => $this->t('Log all salesforce events'),
      ],
      '#default_value' => $config->get('log_level'),
    ];

    $form['log_push_params'] = [
      '#title' => $this->t('Log push params sent to salesforce'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('log_push_params') ?? FALSE,
    ];

    $form['log_push_params_maxlength'] = [
      '#title' => $this->t('Log push params max length'),
      '#type' => 'number',
      '#default_value' => $config->get('log_push_params_maxlength') ?? NULL,
    ];

    $form['log_push_params_sanitized_fields'] = [
      '#title' => $this->t('Fields that must not be shown in logs'),
      '#type' => 'textarea',
      '#default_value' => implode("\r\n", $config->get('log_push_params_sanitized_fields') ?? []),
    ];

    $form['log_push_success'] = [
      '#title' => $this->t('Log push success when an entity is sent to salesforce'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('log_push_success') ?? FALSE,
    ];

    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('salesforce_logger.settings');
    $config->set('log_level', $form_state->getValue('log_level'));
    $config->set('log_push_params', $form_state->getValue('log_push_params'));
    $config->set('log_push_params_maxlength', (int) $form_state->getValue('log_push_params_maxlength'));
    $config->set('log_push_params_sanitized_fields', explode("\r\n", $form_state->getValue('log_push_params_sanitized_fields')));
    $config->set('log_push_success', $form_state->getValue('log_push_success'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
