<?php

namespace Drupal\salesforce_logger\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use Drupal\salesforce\Event\SalesforceEvents;
use Drupal\salesforce\Event\SalesforceExceptionEventInterface;
use Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Salesforce Logger Subscriber.
 *
 * @package Drupal\salesforce_logger
 */
class SalesforceLoggerSubscriber implements EventSubscriberInterface {

  const EXCEPTION_MESSAGE_PLACEHOLDER = '%type: @message in %function (line %line of %file).';

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

   /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * Create a new Salesforce Logger Subscriber.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory.
   */
  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $config) {
    $this->logger = $logger;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      SalesforceEvents::PUSH_PARAMS => 'salesforcePushParams',
      SalesforceEvents::PUSH_SUCCESS => 'salesforcePushSuccess',
      SalesforceEvents::ERROR => 'salesforceException',
      SalesforceEvents::WARNING => 'salesforceException',
      SalesforceEvents::NOTICE => 'salesforceException',
    ];
    return $events;
  }

  /**
   * Log params pushed to salesforce.
   *
   * This functionality can be setup by configuration so that:
   *   - The sensitive fields are not saved into database.
   *   - When the data sent is too large, it can be truncated.
   *
   * @param \Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent $event
   *   Salesforce push params event.
   */
  public function salesforcePushParams(SalesforcePushParamsEvent $event) {
    $settings = $this->config->get('salesforce_logger.settings');
    $log_push_params = (bool) $settings->get('log_push_params');
    if ($log_push_params) {
      $fields_to_sanitize = $settings->get('log_push_params_sanitized_fields') ?? [];
      $params = $event->getParams()->getParams();
      foreach ($params as $key => $value) {
        if (in_array($key, $fields_to_sanitize)) {
          $params[$key] = '****';
        }
      }

      $max_length = $settings->get('log_push_params_maxlength');
      $params_json = json_encode($params);
      if (!empty($max_length) && strlen($params_json) > $max_length) {
        $params_json = substr($params_json, 0, $max_length) . '...';
      }

      $this->logger->debug(
        sprintf('Entity of type "%s" is being pushed to salesforce "%s" entity. Drupal entity ID: %s. Data: %s',
          $event->getEntity()->getEntityTypeId(),
          $event->getMapping()->getSalesforceObjectType(),
          $event->getEntity()->id(),
          $params_json
        )
      );

    }
  }

  /**
   * Logs when a entity is mapped successfully to salesforce.
   *
   * @param \Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent $event
   *   Push params event with salesforce ID.
   */
  public function salesforcePushSuccess(SalesforcePushParamsEvent $event) {
    $log_push_params = (bool) $this->config->get('log_push_success');
    if ($log_push_params) {
      $this->logger->info(
        sprintf(
          'Entity of type "%s" has been succesfully sent to salesforce as a "%s" entity. Drupal entity ID: %s. Salesforce entity ID: %s.',
          $event->getEntity()->getEntityTypeId(),
          $event->getMapping()->getSalesforceObjectType(),
          $event->getEntity()->id(),
          $event->getMappedObject()->sfid()
        )
      );
    }
  }

  /**
   * SalesforceException event callback.
   *
   * @param \Drupal\salesforce\Event\SalesforceExceptionEventInterface $event
   *   The event.
   */
  public function salesforceException(SalesforceExceptionEventInterface $event) {
    $log_level_setting = $this->config->get('salesforce_logger.settings')->get('log_level');
    $event_level = $event->getLevel();
    // Only log events whose log level is greater or equal to min log level
    // setting.
    if ($log_level_setting != SalesforceEvents::NOTICE) {
      if ($log_level_setting == SalesforceEvents::ERROR && $event_level != RfcLogLevel::ERROR) {
        return;
      }
      if ($log_level_setting == SalesforceEvents::WARNING && $event_level == RfcLogLevel::NOTICE) {
        return;
      }
    }

    $exception = $event->getException();
    if ($exception) {
      $this->logger->log($event->getLevel(), self::EXCEPTION_MESSAGE_PLACEHOLDER, Error::decodeException($exception));
    }
    else {
      $this->logger->log($event->getLevel(), $event->getMessage(), $event->getContext());
    }
  }

}
